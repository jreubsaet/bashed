# Bashed Reborn
This project is a set of scripts that add extra functionality to your Bash shell. It contains the following components:

* `core`: this contains the core scripts that most of Bashed's components rely on.
* `greeters`: a set of scripts that show information about available updates, orphaned and cached packages, and misc. system information.
* `aliases`: a set of command aliases to 'enable by default' command-line switches, or functions to replace commands entirely.


## Important: Update 2020-08-02
This update is a **major** refactoring and reorganization of the Bashed Reborn codebase. The pre-refactor codebase has been branched off to form the branch `release-0.1`. Track this branch if you wish to stay on the older version of Bashed Reborn. This version will see some maintenance, but will not get overhauled or new features added to it.  
The `master` branch will from now on contain the refactored codebase.  
For more information, see tag `release-0.2`.


### Prerequisites
These scripts have been written with Arch Linux in mind. While they should at least execute on other distributions, they may not show all information, or throw up error messages.  
**NOTE**: I did not add in 'sanity checks' to make it run error-free on other distributions or other configurations.

#### Software
For the best results, install following tools and programs:

* `aurutils`: AladW's AUR utilities. Create a local repository `aur` for your AUR packages, and one called `custom` for your custom packages, to make the AUR update checker and custom package query work, respectively.
* `expac`: This utility allows querying pacman's databases and outputs the information in a user-choosable string. Used to check for AUR package updates (using the local `aur` repository) and to query installed AUR and custom packages.
* `expect`: This package provides `unbuffer`, which retains formatting when piping command output into another.
* `git`: Download and update this project using `git`.
* `pacman-contrib`: This package provides `checkupdates`, which is used to check for repository package updates.

#### Configuration
* I run my Bash shell as a login shell (changed in my terminal emulator's settings). Through the `su` alias, my root shell (when invoked through `su`) is also run as a login shell. This way, `/etc/profile` gets sourced whenever you run Bash, which dynamically updates variables to reflect the user change (e.g. I set `$XDG_*_HOME` dynamically through `/etc/profile`). This way, only a `~/.bash_profile` is necessary.
* To get a proper output for custom packages and AUR packages, set up local repositories named `custom` and `AUR`.

### Install Instructions
1. Clone this repository into `$XDG_CONFIG_HOME` (or if not set, `~/.config`), which will create the `bashed` subdirectory.
2. Add the following line to `~/.bash_profile` (login shells) or `~/.bashrc` (normal interactive shells): `source $XDG_CONFIG_HOME/bashed/bashedrc`.
3. Open a terminal. If all prerequisites are met, Bashed Reborn should display properly.

### Updates
To get updates to this project, run `git pull` in `$XDG_CONFIG_HOME/bashed`.  
If you wish to track the `release-0.1` branch, run `git checkout release-0.1` instead.


#### Screenshots
![Bashed Reborn with a dark-on-light color scheme](scrots/bashed-light.png "Bashed Reborn, showing all greeters")

![Bashed Reborn with a light-on-dark color scheme](scrots/bashed-dark.png "Bashed Reborn, showing all greeters")
